/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package person.boot.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import person.boot.dao.PersonDAO;
import person.boot.domain.Person;

/**
 *
 * @author Ricard
 */
@Service
public class personServiceImpl implements PersonService{
    
    @Autowired
    private PersonDAO PersonDAO;
    
    @Override
    public Person savePerson(Person person) {
        return PersonDAO.save(person);
    }
    
    @Override
    public List<Person> getAllPersons(){
        return PersonDAO.findAll();
    }
    
    @Override
    public Person getPersonById(Long id){
        return PersonDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Person findPerson(Person person) {
        return PersonDAO.findById(person.getId()).orElse(null);
    }
    
    @Override
    @Transactional
    public void deletePerson(Long id) {
        Person person = PersonDAO.findById(id).orElse(null);
        PersonDAO.delete(person);
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Person> getEmailsEndingWithGmail(){
        return PersonDAO.findEmailsEndingWithGmail();
    }
}
