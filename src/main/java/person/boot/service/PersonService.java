/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package person.boot.service;

import java.util.List;
import person.boot.domain.Person;

/**
 *
 * @author Ricard
 */
public interface PersonService {
    Person savePerson(Person person);
    public List<Person> getAllPersons();
    public Person getPersonById(Long id);
    public Person findPerson(Person person);
    public void deletePerson(Long id);
    public List<Person> getEmailsEndingWithGmail();
}
