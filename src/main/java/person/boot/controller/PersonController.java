/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package person.boot.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import person.boot.domain.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import person.boot.service.PersonService;

/**
 *
 * @author Albert Grau
 * http://localhost:8080/person
 */
@Controller
public class PersonController {

    @Autowired
    private PersonService PersonService;
    
    @GetMapping("/person")
    public String showForm(Person person) {
        return "person-form";
    }
    
    @PostMapping("/person")
    public String submitForm(Person person, Model model) {
        PersonService.savePerson(person);
        return "person-result";
    }
    
    @GetMapping("/persons")
    public String listPersons(Model model) {
        List<Person> persons = PersonService.getAllPersons();
        model.addAttribute("persons", persons);
        return "person-list";
    }
    
    @GetMapping("/update/{id}")
    public String update(Person person, Model model){
        person = PersonService.findPerson(person);
        model.addAttribute("person",person);
        return "person-form";
    }
    
    @GetMapping("/delete/{id}")
    public String showDeleteConfirmation(@PathVariable Long id, Model model){
        Person person = PersonService.getPersonById(id);
        model.addAttribute("person",person);
        return "person-delete-confirmation";
    }
    
    @GetMapping("/deleteOK/{id}")
    public String deletePerson(Person person){
        PersonService.deletePerson(person.getId());
        return "redirect:/persons";
    }
    
    @GetMapping("/emails-with-gmail")
    public String getEmailsEndingWithGmail(Model model){
        List<Person> emails = PersonService.getEmailsEndingWithGmail();
        model.addAttribute("persons",emails);
        return "person-list";
    }
}