/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package person.boot.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import person.boot.domain.Person;

/**
 *
 * @author Ricard
 */
public interface PersonDAO extends JpaRepository<Person, Long>{
    
    @Query("SELECT p FROM Person p WHERE p.email LIKE '%@gmail.com'")
    List<Person> findEmailsEndingWithGmail();
}
